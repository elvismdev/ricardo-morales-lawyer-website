<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rmoraleslaw');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_L0{}LTuxF{x94<6XNES mOM$}lFpv-I X>RaJ/lpo5:&(vtdrX{R||oUPJ|M`ju');
define('SECURE_AUTH_KEY',  'tLo)4WAb{}3s_K[FA?(:Oz3d~e*YnBe#?Y4|q-WAWi/3&l80+bV]`*-=vEI_IJ~s');
define('LOGGED_IN_KEY',    'Gv*ofBw-?$G1/p+kKTHY(q=n,jg9a(ZG$jd6U8-wDhoQrx:L]nSivj$(4xJFAi3[');
define('NONCE_KEY',        '33(MGJ>}=3N?4VHE+e -v~`wCZmpDlEl6-#IOcJ TuFPDI=BeR{JTm#Z9aEkkt_$');
define('AUTH_SALT',        'fCsP|b&;!U-&M,_A5|;o!mo%(MFXfAFe/;iW~swrq{49F6$KQ:pT{~rM|[DNQGbd');
define('SECURE_AUTH_SALT', 'bj@!yUt>+BJ<|9A]P_%vN-fkA$r&<D+|=R&Y|4 &;Z.Iu;9-4h=T+K)nxt:GkA#>');
define('LOGGED_IN_SALT',   '<-n.tH*~him]_~{,NCz i)Q<#IvoEtMt;+qk)T40#X1fgKOa_s/H%0u0aUyYc36O');
define('NONCE_SALT',       'u5/Hla+^p8oz]IYN+{4VEBe> Hj3;0j;a, qX2AAdRf]_bW@7#>{UcL{>d9$cj43');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');